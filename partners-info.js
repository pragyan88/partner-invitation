const fs = require('fs');
const path = require('path');

/**
 * get all the partners from specified fileName
 * @param {object} config
 * @param {function} cb
 */
const getPartners = (config, cb) => {
  const { fileName } = config;
  const filePath = path.join(__dirname, fileName);
  fs.readFile(filePath, (err, data) => {
    try {
      const partners = JSON.parse(data);
      cb(null, partners);
    } catch (err) {
      cb(err);
    }
  });
};

module.exports = getPartners;
