
/**
 * converts degree scale to radian scale
 * @param {number} num
 * @returns {number}
 */
const toRadians = num => num * (Math.PI / 180);

/**
 * calculate the distance between source and destination in kilometers
 * @param {any} source coordinates of the source
 * @param {any} destination coodinates of the destination
 * @returns {number} distance between source and destination in kilometers
 */
const calculateDistance = (source, destination) => {
  const { lat: lat1, lon: lon1 } = source;
  const { lat: lat2, lon: lon2 } = destination;

  const R = 6371; // radius of earth in kilometers
  const φ1 = toRadians(lat1);
  const φ2 = toRadians(lat2);
  const Δφ = toRadians((lat2 - lat1));
  const Δλ = toRadians((lon2 - lon1));

  const a = (Math.sin(Δφ / 2) * Math.sin(Δφ / 2)) + (Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2));
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  const d = R * c;

  return d;
};

module.exports = calculateDistance;
