
const csvToCoordinates = require('./csv-to-coordinates');
const calculateDistance = require('./calculate-distance');

/**
 * check if office distance is greater than specified distance
 * @param {object} config
 * @param {object} office
 * @returns {boolean}
 */
const filterOffice = config => (office) => {
  const { sourceCoordinates, distanceOfSource: distance } = config;
  const officeDistance = calculateDistance(sourceCoordinates, csvToCoordinates(office.coordinates));
  return officeDistance <= distance;
};

/**
 * check if address exists for partner
 * @param {Array} partner
 * @returns {boolean}
 */
const filterPartner = (partner) => {
  if (partner.addresses.length) {
    return true;
  }
  return false;
};

module.exports = { filterOffice, filterPartner };
