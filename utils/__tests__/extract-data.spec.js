const chalk = require('chalk');
const chai = require('chai');
const { extractData } = require('../extract-data');

const expect = chai.expect;

describe(chalk.blue('extract partner data'), () => {
  it('extracts partnerData with empty address if partner does not satisfy cofiguration', (done) => {
    const config = { sourceCoordinates: { lat: 51.515419, lon: -0.141099 }, distanceOfSource: 100 };
    const partnerInfo = {
        "id": 2,
        "urlName": "spring-development",
        "organization": "Spring Development",
        "customerLocations": "across the UK",
        "willWorkRemotely": true,
        "website": "http://www.springdevelopment.net/",
        "services": "We provide training, coaching and consultancy to ensure that 360 feedback is delivered positively and constructively, maximising personal development. We can train your people to carry out effective feedback meetings or, if you would benefit from having external, impartial facilitators, we can come and do them for you. We're always happy to have an initial confidential discussion to explore how we can help you.",
        "offices": [
        {
            "location": "Banbury, Oxfordshire",
            "address": "Banbury, Oxfordshire",
            "coordinates": "52.0629009,-1.3397750000000315"
        }
        ]
    };
    const partnerData = extractData(config)(partnerInfo);
    expect(partnerData).to.eql({ name: 'Spring Development', addresses: [] });
    expect(partnerData.addresses.length).equal(0);
    done();
  });

   it('extracts partnerData with address if partner satisfies cofiguration', (done) => {
    const config = { sourceCoordinates: { lat: 51.515419, lon: -0.141099 }, distanceOfSource: 100 };
    const partnerInfo = {
        "id": 4,
        "urlName": "blue-square-360",
        "organization": "Blue Square 360",
        "customerLocations": "globally",
        "willWorkRemotely": true,
        "website": "http://www.bluesquare360.com/",
        "services": "Blue Square 360 provides a professionally managed service covering all areas of a 360° Feedback initiative. We're experienced in supporting projects of all sizes, and always deliver a personal service that provides the level of support you need to ensure your 360 initiative delivers results for the business.",
        "offices": [
        {
            "location": "Singapore",
            "address": "Ocean Financial Centre, Level 40, 10 Collyer Quay, Singapore, 049315",
            "coordinates": "1.28304,103.85199319999992"
        },
        {
            "location": "London, UK",
            "address": "St Saviours Wharf, London SE1 2BE",
            "coordinates": "51.5014767,-0.0713608999999451"
        }
        ]
    };
    const partnerData = extractData(config)(partnerInfo);
    expect(partnerData).to.eql({ name: 'Blue Square 360', addresses: [ 'St Saviours Wharf, London SE1 2BE' ] });
    expect(partnerData.addresses.length).equal(1);
    done();
  });
});