
const config = {
  fileName: 'partners.json',
  sourceCoordinates: { lat: 51.515419, lon: -0.141099 }, // coordinates for central London
  distanceOfSource: 100, // in kilometers
};

module.exports = config;
