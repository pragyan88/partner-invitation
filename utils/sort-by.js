
/**
 * sort helper for sorting array of objects by name
 * @param {object} prev
 * @param {object} next
 */
const sortByName = (prev, next) => prev.name - next.name;

module.exports = { sortByName };
