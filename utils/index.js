
const { filterPartner } = require('./filter');
const { sortByName } = require('./sort-by');
const { extractData } = require('./extract-data');

module.exports = Object.assign({}, { extractData, filterPartner, sortByName });

