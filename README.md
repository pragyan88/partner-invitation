# About

Program to find partners with offices within 100km of central London (coordinates 51.515419, -0.141099) for meal invitation.

# Getting Started

* Set Configuation : In the ***config.js*** file add the specified configuration.
    ```
    fileName: 'partners.json',
    sourceCoordinates: { lat: 51.515419, lon: -0.141099 }, // coordinates for central London
    distanceOfSource: 100
    ```
* Execute Program : 

    ```

    $ node .

    ``` 

# Development & Testing

* Install development dependencies :

    ```

    $ npm install

    ```
* Lint files
    ```
    $ npm run lint
    ```

* Testing with mocha :

    ```
    $ npm test

    ```

# Distance Calculation

This uses the ***haversine*** formula to calculate the great-circle distance between two points – that is, the shortest distance over the earth’s surface.

* ***Haversine formula*** :	
    ```
    a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
    c = 2 ⋅ atan2( √a, √(1−a) )
    d = R ⋅ c
    ```
    where	φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);

    Note that angles need to be in radians.

* Refer to [Great-Circle distance - Wikipedia](https://en.wikipedia.org/wiki/Great-circle_distance?inf_contact_key=7d77e99a71da21e297213b6583a217d81e7d23a694ddccb778445f591fd85edb)


