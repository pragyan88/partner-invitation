const chalk = require('chalk');
const chai = require('chai');
const { filterOffice, filterPartner } = require('../filter');

const expect = chai.expect;

describe(chalk.blue('filter partner with address and filter office with distance'), () => {
  it('filterOffice returns false for the specified configuration', (done) => {
    const config = { sourceCoordinates: { lat: 51.515419, lon: -0.141099 }, distanceOfSource: 100 };
    const office = {
            "location": "Banbury, Oxfordshire",
            "address": "Banbury, Oxfordshire",
            "coordinates": "52.0629009,-1.3397750000000315"
        };
    const lte100km = filterOffice(config)(office);
    expect(lte100km).to.equal(false);
    done();
  });

   it('filterOffice returns true for the specified configuration', (done) => {
    const config = { sourceCoordinates: { lat: 51.515419, lon: -0.141099 }, distanceOfSource: 100 };
    const office = {
            "location": "London, UK",
            "address": "St Saviours Wharf, London SE1 2BE",
            "coordinates": "51.5014767,-0.0713608999999451"
        };
    const lte100km = filterOffice(config)(office);
    expect(lte100km).to.equal(true);
    done();
  });

  it('filterPartner returns false for empty partner address list', (done) => {
    const config = { sourceCoordinates: { lat: 51.515419, lon: -0.141099 }, distanceOfSource: 100 };
    const partner = {
            "name": "Talent Lab",
            "addresses": []
        };
    const addressExists = filterPartner(partner);
    expect(addressExists).to.equal(false);
    done();
  });

  it('filterPartner returns true for non-empty partner address list', (done) => {
    const config = { sourceCoordinates: { lat: 51.515419, lon: -0.141099 }, distanceOfSource: 100 };
    const partner = {
            "name": "Spring Development",
            "addresses": ["Banbury, Oxfordshire"]
        };
    const addressExists = filterPartner(partner);
    expect(addressExists).to.equal(true);
    done();
  });
});