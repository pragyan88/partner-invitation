
const { filterOffice } = require('./filter');

 /**
 * extracts partner name and address from partnerInfo
 * @param {object} partnerInfo
 * @returns {object}
 */
const extractData = config => (partnerInfo) => {
  const name = partnerInfo.organization;
  const offices = partnerInfo.offices;
  const officesWithinDistance = offices.filter(filterOffice(config));
  const addresses = officesWithinDistance.map(office => office.address);
  return { name, addresses };
};

module.exports = { extractData };
