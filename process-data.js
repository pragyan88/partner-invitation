
const { extractData, filterPartner, sortByName } = require('./utils');

/**
 * process array of partners for the required data
 * @param {object} config
 * @param {Array} partners
 * @returns {Array}
 */
const processData = (config, partners) => partners.map(extractData(config)).filter(filterPartner).sort(sortByName);

module.exports = processData;
