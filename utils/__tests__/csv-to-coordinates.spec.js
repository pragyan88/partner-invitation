const chalk = require('chalk');
const chai = require('chai');
const csvToCoord = require('../csv-to-coordinates');

const expect = chai.expect;

describe(chalk.blue('csv to coordinates conversion'), () => {
  it('converts csv value to coordinates object', (done) => {
    const csv = '51.515419, -0.141099';
    const coordinates = csvToCoord(csv);
    expect(coordinates).to.eql({ lat:51.515419, lon:-0.141099 });
    done();
  });
});