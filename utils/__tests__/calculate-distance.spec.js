const chalk = require('chalk');
const chai = require('chai');
const calculate = require('../calculate-distance');

const expect = chai.expect;

describe(chalk.blue('Calculate distance test'), () => {
  it('calculates disance between coordinates', (done) => {
    const source = { lat: 51.515419, lon: -0.141099 };
    const destination = { lat: 52.0629009, lon: -1.3397750000000315 };
    const distance = calculate(source, destination);
    expect(distance).to.equal(102.48330298955042);
    done();
  });
});