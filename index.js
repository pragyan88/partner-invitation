const config = require('./config');
const getPartners = require('./partners-info');
const processData = require('./process-data');

getPartners(config, (err, partners) => {
  if (err) {
    console.error(err); // eslint-disable-line no-console
  }
  const info = processData(config, partners);
  console.log(info); // eslint-disable-line no-console
});

