
/**
 * converts comma separated values to lat lon coordinates
 * @param {string} csv
 * @returns {object}
 */
const csvToCoordinates = (csv) => {
  const num = csv.split(',');
  return { lat: parseFloat(num[0], 10), lon: parseFloat(num[1], 10) };
};

module.exports = csvToCoordinates;
